# FDrums
FDrums is a DIY electronic drum set that uses a combination of mechanical and electronic components to create a unique sound. The project includes designing and building the drum pads, as well as creating software for controlling the instrument's sound.

## Drum Pads
The drum pads are designed using CAD software and can be 3D printed or fabricated using other methods. They use piezo speakers to create a responsive and expressive playing experience. The project also includes a software component that allows users to control the instrument's sound using MIDI controllers.

## Software
The software component of the project includes programming for controlling the instrument's sound using MIDI controllers, as well as creating custom sounds and effects. The project also includes links to various resources related to MIDI controllers and software development.

## Notes
general:
- optimize flex on cr10
- test soldering on piezodisk with minijack cables

snare:
- improve v0: more travel, adjust hight
- cad pivot / hinge

bassdrum / hihat:
- cad better target, maybe adjustable
- print new beater version
- try td3 settings
