/*
  EXAMPLE - Simple Piezo Sensing

  With this sample code, you will make snare and tom using two piezo.
  https://github.com/RyoKosaka/HelloDrum-arduino-Library
*/

///////////////////////////////   SETTING VALUE   ///////////////////////////////////

//Determine the setting value.
//By changing the number in this array you can set sensitivity, threshold and so on.
// curve type: 0: lin, 1: exp1, 2: exp2, 3: log1, 4: log2
//
//
// byte EXAMPLE[6] = {
//     100, //sensitivity (1-100)
//     100,  //threshold (1-100)
//     10,  //scan time (1-)
//     100,  //mask time (1-)
//     XX,  //note (0-127)
//     0    //curve type (0-4)
// };

// efdrums pedal/bar setup
// pedals: byte KICK[6] = { 100, 40,  10,  100, 36,  1};
// bars: byte SNARE[6] = { 50, 30, 10, 100, 38, 0};

byte KICK[6] = {
    80, //sensitivity (1-100)
    22,  //threshold (1-100)
    10,  //scan time (1-)
    100,  //mask time (1-)
    36,  //note (0-127)
    4    //curve type (0-4)
};

byte SNARE[6] = {
    70, //sensitivity (1-100)
    22,  //threshold (1-100)
    10,  //scan time (1-)
    100,  //mask time (1-)
    38,  //note (0-127)
    3    //curve type (0-4)
};

byte HIHAT[6] = {
    80, //sensitivity (1-100)
    20,  //threshold (1-100)
    10,  //scan time (1-)
    100,  //mask time (1-)
    42,  //note (0-127)
    4    //curve type (0-4)
};

byte CRASH[6] = {
    70, //sensitivity (1-100)
    22,  //threshold (1-100)
    10,  //scan time (1-)
    100,  //mask time (1-)
    57,  //note (0-127)
    3    //curve type (0-4)
};


/////////////////////////////////////////////////////////////////////////////////////

#include <hellodrum.h>

//Using MIDI Library. If you want to use USB-MIDI, comment out the next two lines.
#include <MIDI.h>
MIDI_CREATE_DEFAULT_INSTANCE();

//Uncomment the next two lines for using USB-MIDI with atmega32u4 or Teensy
//#include <USB-MIDI.h>
//USBMIDI_CREATE_DEFAULT_INSTANCE();

// serial MIDI pins
// #define RXD2 16
// #define TXD2 17

//Please name your piezo.
//The piezo named snare is connected to the A0 pin and the piezo named tom is connected to the A1 pin.

HelloDrum kick(35);
HelloDrum snare(34);
HelloDrum hihat(32);
HelloDrum crash(33);

// void midiCommand(byte cmd, byte data1, byte  data2) {
//   Serial.write(cmd);     // command byte (should be > 127)
//   Serial.write(data1);   // data byte 1 (should be < 128)
//   Serial.write(data2);   // data byte 2 (should be < 128)
  // Serial.print(cmd);
  // Serial.print(data1);
  
  // Serial.println(data2);
  // Serial2.write(cmd);     // command byte (should be > 127)
  // Serial2.write(data1);   // data byte 1 (should be < 128)
  // Serial2.write(data2);   // data byte 2 (should be < 128)
// }

void setup()
{
  //If you use Hairless MIDI, you have to comment out the next line.
  //MIDI.begin(10);

  //And uncomment the next two lines.
  MIDI.begin(10);
  Serial.begin(115200);
  // Serial2.begin(31250, SERIAL_8N1, RXD2, TXD2);
  // Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2);

  //Set Curve Type
  kick.setCurve(KICK[5]);
  snare.setCurve(SNARE[5]);
  hihat.setCurve(HIHAT[5]);
  crash.setCurve(CRASH[5]);
}

void loop()
{
  //Piezo sensing is done in this line. And it is returned as a velocity of 127 stages.
  //For each piezo, one line is required.
  kick.singlePiezo(KICK[0], KICK[1], KICK[2], KICK[3]);
  snare.singlePiezo(SNARE[0], SNARE[1], SNARE[2], SNARE[3]);
  hihat.singlePiezo(HIHAT[0], HIHAT[1], HIHAT[2], HIHAT[3]);
  crash.singlePiezo(CRASH[0], CRASH[1], CRASH[2], CRASH[3]);

  //MIDI signals are transmitted with this IF statement.
  //For each piezo, one IF statement is required

  if (kick.hit == true)
  {
    MIDI.sendNoteOn(KICK[4], kick.velocity, 10); //(note, velocity, channel)
    MIDI.sendNoteOff(KICK[4], 0, 10);
  }
  if (snare.hit == true)
  {
    MIDI.sendNoteOn(SNARE[4], snare.velocity, 10); //(note, velocity, channel)
    MIDI.sendNoteOff(SNARE[4], 0, 10);
  }
 if (hihat.hit == true)
 {
   MIDI.sendNoteOn(HIHAT[4], hihat.velocity, 10); //(note, velocity, channel)
   MIDI.sendNoteOff(HIHAT[4], 0, 10);
 }
  if (crash.hit == true)
  {
    MIDI.sendNoteOn(CRASH[4], crash.velocity, 10); //(note, velocity, channel)
    MIDI.sendNoteOff(CRASH[4], 0, 10);
  }

}
