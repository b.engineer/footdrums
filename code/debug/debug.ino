#include <Arduino.h>

void setup() {
 // Initialize serial communication at 115200 bits per second:
 Serial.begin(115200);
  
 // Set the resolution to 12 bits (0-4096)
 analogReadResolution(12);
}

void loop() {
 // Read the analog / millivolts value for pins 32, 33, 34, and 35:
 int analogValue34 = analogRead(34);
 int analogValue35 = analogRead(35);
 int analogValue32 = analogRead(32);
 int analogValue33 = analogRead(33);

 // Print out the values you read:
Serial.print("34:" );
Serial.print(analogValue34);
Serial.print(",");
Serial.print("35:");
Serial.print(analogValue35);
Serial.print(",");
Serial.print("32:");
Serial.print(analogValue32);
Serial.print(",");
Serial.print("33:");
Serial.println(analogValue33);

 delay(100); // Delay in between reads for clear read from serial
}
