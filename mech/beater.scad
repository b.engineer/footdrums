include <BOSL/constants.scad>
use <BOSL/shapes.scad>

$fn = 200;


h = 42;
sen_d = 35;
sen_h = 0.5;

m_hole = 1;			// mount hole [0,1]
mx = 10;
my = 15;
rod_d = 6.5;

hit_r = 100;
hit_dist = 12;
hit_w = 42;
hit_sr = 3;			// strain relief radius

module beater () {
intersection(){
difference(){

union(){
//translate([mx/2,0,0]) cube([mx, my, h], center=true);
translate([0,0,0]) cube([mx, my, h], center=true);

//translate([0,my/2,0]) interior_fillet(l=h, r=mx, orient=ORIENT_Z);
//translate([0,-my/2,0])interior_fillet(l=h, r=mx, orient=ORIENT_Z_270);
translate([-mx/2,my/2,0]) interior_fillet(l=h, r=mx, orient=ORIENT_Z);
translate([-mx/2,-my/2,0])interior_fillet(l=h, r=mx, orient=ORIENT_Z_270);

intersection(){
//translate([hit_r - hit_dist,0,0]) cylinder(h, hit_r, hit_r, center=true, $fn=300);
translate([hit_r - hit_dist,0,0]) sphere(hit_r, $fn=300);
translate([-hit_r,0,0]) sphere(hit_r, $fn=300);
translate([-hit_dist/2,0,0]) cube([hit_dist, hit_w, h], center=true);
}
}



if(m_hole == 1){
cylinder(h + 1, d = rod_d, center=true);					// rod hole
}
translate([-hit_dist/2,0,0]) cube([sen_h, sen_d, h + 1], center=true);		// sensor hole
//translate([rod_d,0,0]) rotate([0,90,0]) cylinder(10, d=2.9, center=true);	// pcb mounting hole

translate([-hit_dist/2,sen_d/2,0]) cylinder(h + 1, d=hit_sr, center=true);
translate([-hit_dist/2,-sen_d/2,0]) cylinder(h + 1,d=hit_sr, center=true);

} //difference
xcyl(d=h*1.1,l=100);
} //intersection
} //module





beater();
