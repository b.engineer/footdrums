include <BOSL/constants.scad>
use <BOSL/transforms.scad>
use <BOSL/shapes.scad>
use <nutsnbolts/cyl_head_bolt.scad>

x1 = 55;
x2 = 75;
y = 55;
z1 = 4;
z2 = 10;

hd1 = 50; // hole distance 1
hd2 = 40; // hole distance 2

mh = 2.5; // mount height


difference(){
hull(){
zmove(z1/2) cube([x1, 0.1 , z1], center=true);
ymove(y) zmove(z2/2) cube([x2, 0.1, z2], center=true);
}

//top holes
back(y-8) left(hd1/2) up(25) hole_through(name="M5", l=50, cld=0.1, $fn=30);
back(y-8) right(hd1/2) up(25) hole_through(name="M5", l=50, cld=0.1, $fn=30);
back(y-8) left(hd1/2) up(4) zrot(90) nutcatch_parallel("M5", clh=0.1);
back(y-8) right(hd1/2) up(4) zrot(90) nutcatch_parallel("M5", clh=0.1);


//bottom holes
back(8) left(hd2/2) up(25) hole_through(name="M5", l=50, cld=-0.2, $fn=30);
back(8) right(hd2/2) up(25) hole_through(name="M5", l=50, cld=-0.2, $fn=30);

//flat spot for pedal
back(y - 16) up(5 + 8.3 - mh) backcube([100,100,10]);
}
