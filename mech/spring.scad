include <BOSL/constants.scad>
use <BOSL/transforms.scad>
use <BOSL/shapes.scad>

$fn = 100;

//=================general dimensions
springdesign = 0;		// 0 = single ellipse, 1 = double ellipse
x = 100;
y = 50;
z = 20;
w = 3;

px = 25;			// width of top and bottum pads
py = w;				// thickness of top and bottum pads
mid = 10;			// mount rod inner diameter
mod = 14;			// mount rod outer diameter

//-------------------------modules-----------------------------------------
module ellipse_section(sx,sy,sz,w){
intersection(){
difference(){
	translate([0,sy/2,0]) resize([sx + w, sy, sz]) cylinder(1);
	translate([0,sy/2,0 - 1]) resize([sx - w, sy - 2*w, sz + 2]) cylinder(1);
}
cube([sx,sy/2,sz]);
}

}

module double_ellipse(sx,sy,sz,w){
	translate([0,0,0]) ellipse_section(sx,sy,sz,w);
	translate([sx, sy,0]) rotate([0,0,180]) ellipse_section(sx,sy,sz,w);
}



//-------------------------main body--------------------------------------
module spring(sdp,xp,yp,zp,wp,pxp,pyp,midp,modp){

difference(){
union(){

// generate spring elements
if (sdp == 0) {
  translate([-xp/2,0,-zp/2]) ellipse_section(xp-pxp, yp, zp, wp);
  translate([-xp/2,0,-zp/2]) mirror([0,1,0]) ellipse_section(xp-pxp, yp, zp, wp);
  translate([xp/2,0,-zp/2]) mirror([1,0,0]) ellipse_section(xp-pxp, yp, zp, wp);
  translate([xp/2,0,-zp/2]) rotate([0,0,180]) ellipse_section(xp-pxp, yp, zp, wp);
}

if (sdp == 1) {
  translate([-xp/2,0,-zp/2]) mirror([0,0,0]) double_ellipse(xp/2, yp/2, zp, wp);
  translate([-xp/2,0,-zp/2]) mirror([0,1,0]) double_ellipse(xp/2, yp/2, zp, wp);
  translate([xp/2,0,-zp/2]) mirror([1,0,0]) double_ellipse(xp/2, yp/2, zp, wp);
  translate([0,-yp/2,-zp/2]) mirror([0,0,0]) double_ellipse(xp/2, yp/2, zp, wp);
}

// edge connections
translate([-xp/2,-wp, -zp/2]) cube([2*wp,2*wp,zp]);
translate([xp/2 - 2*wp,-wp,-zp/2]) cube([2*wp,2*wp,zp]);

// top/bottum pads
translate([0,yp/2-pyp/2,0]) cube([pxp,pyp,zp], center=true);
translate([0,-yp/2+pyp/2,0]) cube([pxp,pyp,zp], center=true);

// top rod mount
difference(){
  translate([0,yp/2 - modp/2 - w,0]) cube([modp, modp, zp], center=true);
  translate([0,yp/2 - modp/2 - w,0]) cylinder(zp + 1, d=midp, center=true);
}
} //union



} //diff

} //module



spring(springdesign,x,y,z,w,px,py,mid,mod);
