include <BOSL/constants.scad>
use <BOSL/transforms.scad>
use <BOSL/shapes.scad>
use <spring.scad>

$fn = 100;

module generate_arc(od, id, z_height, degrees) {
rotate_extrude(angle=degrees, convexity=10)
	translate([id, 0, 0])
	square([od-id, z_height]);
}


//=================general dimensions
springdesign = 0;		// 0 = single ellipse, 1 = double ellipse
x = 100;
y = 50;
z = 20;
w = 3.6;      // 6 * 0.6mm

px = 25;			// width of top and bottum pads
py = w;				// thickness of top and bottum pads
mid = 10;			// mount rod inner diameter
mod = 14;			// mount rod outer diameter

//===============mounting holes
hid = 4;
hod = 15;

union(){
  spring(springdesign,x,y,z,w,px,py,mid,mod);

  translate([0,-y/2+w/2,z/2 + 6])
  difference(){
     cube([px, w, 12], center=true);
     xrot(90) cylinder(z + 1, d=hid, center=true);
  }

  // positiv stop
  z_height = z; // Set desired height
  degrees = 140; // Set desired arc angle
  od = 20;
  id = od - 3;
  translate([0, -od, -z/2]) zrot(20) generate_arc(od, id, z_height, degrees);
}
