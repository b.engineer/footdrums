$fn = 200;
id = 8 + 0.1;
od = 20;
l = 30;
h = 5;

bx = 30;
by = h;
bz = 30;


translate([0,od,-h-1]) difference(){
union(){
cylinder(h, d = od, center=true);
translate([0,-od/2,0]) cube([od,od,h], center=true);
}
cylinder(h+1, d = id, center=true);
}

translate([0,od,h+1]) difference(){
union(){
cylinder(h, d = od, center=true);
translate([0,-od/2,0]) cube([od,od,h], center=true);
}
cylinder(h+1, d = id, center=true);
}

cube([bx,by,bz],center=true);
