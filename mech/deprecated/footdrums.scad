// first file in scad. never printed, never testet


$fn = 100;


w = 100; // x
d = 60; // y
h = 30; // z
h_base = 6;
r_pocket = 20;
h_pocket = 10;

difference() {
union () {
//top
difference() {
	scale ([1,d/w,h/w])
	sphere ( w/2 );

	translate ([0,0,-w/2])
	cube (w, center = true);
}

//base
translate ([0,0,-h_base/2])
linear_extrude(height = h_base, center = true){
	scale ([1,d/w,h/w])
	circle ( w/2 );
}
}


//pocket
translate([0,0,-h_pocket])
linear_extrude(height = h_pocket){
circle(r_pocket);
}
}
