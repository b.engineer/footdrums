$fn = 200;
id = 8 + 0.1;
od = 15;
l = 30;
h = 5;

d_shaft = 1;
a = 30;


module link(idp,odp,lp,hp){
difference(){
union(){
cylinder(hp, d = odp, center=true);
translate([lp,0,0]) cylinder(hp, d = odp, center=true);
translate([lp/2,0,0]) cube([lp, odp/2, hp], center=true);
}

cylinder(hp+1, d = idp, center=true);
translate([lp,0,0]) cylinder(hp+1, d = idp, center=true);
}

}



if (d_shaft == 1){
difference(){
	cylinder(h, d = od, center=true);

	rotate([0,0,a]) difference(){
		cylinder(h + 1, d = id, center=true);
		translate([0,id -1,0]) cube([id, id, h + 2], center=true);
	}
}
}

link(id,od,l,h);
