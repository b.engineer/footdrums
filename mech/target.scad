include <BOSL/constants.scad>
use <BOSL/transforms.scad>
use <BOSL/shapes.scad>
use <nutsnbolts/cyl_head_bolt.scad>
$fn = 50;

// mount
mx = 60;
my = 35;
mz = 18;

// target
a = 45;
tx = 20;
ty = 70;
tz = 50;

// slot
sx = tx + 10;
sy = 50;
sz = 4.1;


translate([mx/2,0,0]) cuboid([mx,my,mz], chamfer = 3);


rotate([0,90-a,0])
translate([0,0,tz/2])
difference(){
cuboid([tx,ty,tz], chamfer = 3);
zmove(tz/4) cuboid([sx,sy,sz], fillet = sz/2);
}
