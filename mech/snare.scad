include <BOSL/constants.scad>
use <BOSL/transforms.scad>

use <spring.scad>;
use <beater.scad>

//==================main spring
sd = 1;		// 0 = single ellipse, 1 = double ellipse
x = 100;
y = 50;
z = 20;
w = 3;

px = 30;			// width of top and bottum pads
py = w;				// thickness of top and bottum pads
mid = 10;			// mount rod inner diameter
mod = 12;			// mount rod outer diameter

spring(sd,x,y,z,w,px,py,mid,mod);

//==================target spring

t_sd = 0;		// 0 = single ellipse, 1 = double ellipse
t_x = 30;
t_y = 15;
t_z = 20;
t_w = 1.8;

t_px = 12;			// width of top and bottum pads
t_py = t_w;				// thickness of top and bottum pads
t_mid = 0;			// mount rod inner diameter
t_mod = 0;			// mount rod outer diameter


translate([0,-y/2 + t_y/2,0]) spring(t_sd,t_x,t_y,t_z,t_w,t_px,t_py,t_mid,t_mod);

//==================beater
difference(){
translate([0,y/2 - 10,0]) rotate([0,0,90]) beater();
translate([0,y/4 + mid/2 -0.5,0]) cylinder(z+1, d = mid, center=true);
}
