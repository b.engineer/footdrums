include <BOSL/constants.scad>
use <BOSL/transforms.scad>
use <BOSL/shapes.scad>
$fn = 80;



tx = 25;	// top x
ty = 15;	// top y
tz = 15;	// top z

rd = 10;	// rail diameter

hx = 7.5;		// hinge thickness
hy = ty;	// hinge width
hz = 40;	// hinge height

bx = 50;	// bottom x
by = 100;	// bottom y
bz = 7;		// bottom z

cf = 2;		// chamfer


//top
difference(){
move([tx/2,0,hz]) cuboid([tx,ty,tz], chamfer=cf, edges=EDGE_TOP_RT+EDGE_TOP_BK+EDGE_TOP_FR+EDGE_FR_RT+EDGE_BK_RT);
move([tx/2,0,hz]) xcyl(d=rd, h=tx);
}

//hinge
zmove(hz/2-tz/4) cuboid([hx,hy,hz-tz/2], align=V_RIGHT);

//bottom
difference(){
//move([bx/2,0,bz/2]) cuboid([bx,by,bz]);
move([bx/2,0,bz/2]) cuboid([bx,by,bz], chamfer=cf, edges=EDGE_TOP_BK+EDGE_TOP_FR+EDGE_TOP_RT);
move([bx, by/2, bz/2]) zcyl(l=bz+1, r=bx-ty/2);
move([bx, -by/2, bz/2]) zcyl(l=bz+1, r=bx-ty/2);
}

//fillets
move([hx,0,bz]) interior_fillet(l=hy, r=15, orient=ORIENT_Y_90);
move([hx,0,hz-tz/2]) interior_fillet(l=hy, r=10, orient=ORIENT_Y_180);

move([hy/4,-hy/2,bz]) interior_fillet(l=hy/2, r=35, orient=ORIENT_X_90);
move([hy/4,hy/2,bz]) interior_fillet(l=hy/2, r=35, orient=ORIENT_X);

//fillet intersects
intersection(){
move([hx,0,bz]) interior_fillet(l=100, r=15, orient=ORIENT_Y_90);
move([hy/4,-hy/2,bz]) interior_fillet(l=100, r=35, orient=ORIENT_X_90);
move([hx,-hy/2,0])interior_fillet(l=100, r=bx-ty/2, orient=ORIENT_Z_270);
}

intersection(){
move([hx,0,bz]) interior_fillet(l=100, r=15, orient=ORIENT_Y_90);
move([hy/4,hy/2,bz]) interior_fillet(l=100, r=35, orient=ORIENT_X);
move([hx,hy/2,0])interior_fillet(l=100, r=bx-ty/2, orient=ORIENT_Z);
}
