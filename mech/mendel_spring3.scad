//use <teardrop.scad> ;

width = 2.5;
height=10;

module quarter()
{

	difference()
	{

		//quarter cylinder
		cylinder(r=15, h=height,$fn=30);

		//inner hollow
		translate([0,0,-1]) cylinder(r=15-width, h=height+2,$fn=30);

		translate([-16,-2,-1]) cube([34,22,16]);
		translate([0,-16,-1]) cube([16,16,16]);

	}

}

module spring()
{

	//first segment of the spring
	quarter();
	//second segment of the spring
	translate([0,-29-2,0]) mirror([0,1,0]) quarter();

	//block between
	translate([-2,-18.6,0]) cube([3.5,6.2,height]);
}

#spring();
#translate([-38.5,-31,0]) rotate([0,0,180]) spring();


//top and bottom of the spring
%difference()
{

	union()
	{
		translate([-25.25,-6,0]) cube([12,4,height]);
		translate([-25.25,-29,0]) cube([12,4,height]);
	}
	translate([-19,-15.5,height/2-height/20]) rotate([0,0,90]) teardrop(2.3,30,90);
	//hole inside
	//translate([-19,-15.5,height/2-height/20]) rotate([0,0,90]) teardrop(2.3,30,90);

}
