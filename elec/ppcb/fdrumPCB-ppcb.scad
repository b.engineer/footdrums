include <BOSL/constants.scad>
use <BOSL/transforms.scad>


difference(){
hull(){
	linear_extrude(height = 1.6, center = false) import("fdrumPCB-Edge_Cuts.svg");
}
up(0.9)linear_extrude(height = 0.7, center = false) import("fdrumPCB-F_Cu.svg");
linear_extrude(height = 0.7, center = false) import("fdrumPCB-B_Cu.svg");
linear_extrude(height = 4, center = false) import("fdrumPCB-F_Mask.svg");
}
